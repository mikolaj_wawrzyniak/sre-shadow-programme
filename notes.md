# 21 Jully

## Notes, thoughs, and ideas:

### Aggregating logs into single events

After we tracked down sidekiq incident we had a chat with @craigf about oppurtunities how to make GitLab more observability friendly, 
he made a greate suggestion that aimed to reduce effort required to explore logs data. Right now in most cases GitLab emmits plentyfull of log lines, 
very often multiple log lines relates to single event eg: 
![multiple_lines_per_event](img/multiple_lines_per_event.png)

In this case we can notice that processing single job emmited 8 different log lines, and whats more important values of this log lines are not obvious to someone that has not indepth konwledge about sidekiq logging, eg: log lines with `job_status`: `fail` do not carry details about the failure, instead there are lines without any (`-`) `job_status` that gives real `error_class` and `error_message` data. Forcing one to explore and discover such details about logging implementation for each monitored services is cumbersome and cost precious times when investigating incident. This can be alleviate if all context data that relates to single event (eg: porocessing a job) will be aggregated into wide entry. Having such aggregation will reduce infomration noise, and provide self explanatory explicit statement about event structre which will be helpful in building right filters to find interesting data. Related article to that topic https://charity.wtf/2019/02/05/logs-vs-structured-events/

Technical approach to this issue:

Reinstrumenting whole GitLab system is way to challenging. However we could explore possibility of using logstash aggregte filter https://www.elastic.co/guide/en/logstash/current/plugins-filters-aggregate.html to aggregate multiple log lines into single more data reach one

### Adding visualisation for data in logs 

During [Sidekiq external_service_reactive_caching](/sidekiq-external_service_reactive_caching) incident I've noticed important gap in ~group::apm tooling, right now we allow users to plot data, that is extracted from prometheus which is equivalent to first step of investigation process, than we allow them to explor logs to get more detailed insights about incident, however we lack any tooling that would help users in the last step which is to visualise data that is fetched from logs. It will be very usefull to enable users to aggregate log lines and plot them on charts to find requrring or dominant patterns in the logs.


## Incidents:

### Sidekiq external_service_reactive_caching error ratio spike

Issue: https://gitlab.com/gitlab-com/gl-infra/production/-/issues/2438

YT video: https://youtu.be/i-aUu_b0x2c

The incident discovery started with alert on one metric from [4 golden signals](https://blog.appoptics.com/the-four-golden-signals-for-monitoring-distributed-systems/) namely error ration which spiked above 16% when normaly is should be belowe 1.5% 
The very first step taken in process of investigation was visiting Grafana dashboard which presents charts that shows highly aggregated data about services perfomrance. This data gives general overview of current state of monitored services or applications, and allows to spot anomallies, but the tradoff made for sake of high aggregation is that this data lacks on details, and except from notifying about mallfunction, it is not very usefull in determing what is the root cause of a malfunction, and posibbly what are its outcomes.

In order to fill metioned gaps in data, the next step is performed, that is going to the (slice of the) logs (at incident span time period) of monitored services and browsing them in search of any unwanted or expected entries. This step is challenging, because there is no unified structure of the logs even at the level of single service, and one event can result in multpile log lines (see [Aggregating logs into single events
](/aggregating-logs-into-single-events)) that forces SRE engineer to build complex filters to find interesting messages. It is done by iteratively adding new filters that removes more and more standard logs lines from analyzed scope. 

Than when most of unrelated log lines are out of the analysed scope a visualistation (in for of a graph) is build, based on that person investigating issue can judge what are the events that triggers the annomally, if they are similiar, is there any pattern in them etc. Than information is used to decide about next steps in resolving the incident. It can either point to specific parts of codebase that are malfunctiong, or indicate that some parts of infrastructure is not working and so on. 

### Sidekiq mailer queue error rates out of SLO 

Issue

Incident was discovered through alert at #feed_alerts-general. It reported that mailers queue has error ration above 9.43%. First step towards finding root cause was to visit grafana dashboard with general high level overview metrics data, based on [4 golden signals](https://blog.appoptics.com/the-four-golden-signals-for-monitoring-distributed-systems/). That allowed to confirm that indeed error ratio spiked out, and when this spike occured.

Than investigation moved to kibana discover view. At that point investigation was focused on exploring logs, learning it format and building filter set to slice data in a way that it will only include data that contributes to error ratio spike, and that there is only one log line per single event. Than such preprared dataset is used to buidl aggregations and visualization.   

### Stuck migration 

Deployments to prod and stg were stuck because opearation exectuded inside AR migration required higher user permissinos than the user that executes migration has (namely super user permissions)

https://gitlab.com/gitlab-com/gl-infra/production/-/issues/2439

solved by runing stuck operation as super user on psql cli, and re running migration. Not much investigation done. 

# 22 July

## Notes, thoughs, and ideas:

### How build roubust alerting and not go crazy

Typical approach to alerting is based on alerting on causes, therefor watching for infrastructure indicators, like CPU or memory staturation, disc usage etc. In larger fleets that approach tendt to produce high amount of notification, which not necessarly indicates any negative implication for users, and may get back to right state on their own. Chasing causes of each of such alerts can take attention of SRE from other issues, that may have user facing effects. 

Alternative approach is focused on setting alerts on use facing symptoms, on service layer, such as latency or error rate. Each of such alerts indicates sever situattion that affects end users, and should be given attention. Regardless the fact that alerting system is build based on user facing metrics, infrastracture ones should be mesured as well.
When alert is fired SRE engineer visits dashboard presenting both of metrics types, and check if there is any corelation between them, to indicate if user facing symptoms are caused by infrastructure malfunction.

## Incidents

### sidekiq_throttled_jobs_enqueued_without_dequeuing

Possibly some performance testing may give false positive result on this alert.
Another thing is that this alrets seems to be watching over queues pilling up, but with zero tolerance, upon directly measuring queue sieze, nothing suspicous came up, also other 2 queues that was firing it, seemed to to have very low up to zero enqueue rate. 

Steps to taken to investigate: Alert message didn't contain much helpful details. In order to investigate root cause of an issue, following steps were taken:

1. Visiting alerts manager (alerts.gitlab.net) for more details
1. Switching to [Thanos](https://thanos-query.ops.gitlab.net) to query raw data about alerts, using query: `ALERTS{alertstate="firig", alertname="sidekiq_throttled_jobs_enqueued_without_dequeuing",env="gprd"}` and grab queue names that has triggered alerts
1. Visit graphana `Queue Detail` dashboard and select one of the affected queues - non of signals shows any indication of anomaly
1. Check alerts definition to make sure nothing is overlooked, and better understand what is beeing alerted on. Located promethues instance which sends out the alerts out of alerts labes in thanos query result (`prometheus-app.gprd.gitlab.net/alerts`). Since alert definiton was lengthy, runbooks repo (https://gitlab.com/gitlab-com/runbooks/-/blob/master/rules-jsonnet/sidekiq-worker-key-metrics.jsonnet#L133) was visited to view better formatted version.
1. To double check alert query was plotted over graph, and it does not indicate any problems
1. Alert was declared as false positive    


###   The `registry` service, `loadbalancer_cny` component, `cny` stage, has an error burn-rate exceeding SLO

1. Visited https://dashboards.gitlab.net/d/registry-main/registry-overview?orgId=1&var-PROMETHEUS_DS=Global&var-environment=gprd&var-stage=cny&var-sigma=2 
1. Noticed that RPS (request per second) is very low around 1
1. Decided that this is false positive, as error brun rate back to normal, and for low RPS services that often means that pod got restarted and some helthecheck failed temporarly

# 23 July


## Notes, thoughs, and ideas:

### Advantages that has promethues over elastic, and why to use both or even prometheus alone over elk 

Craig explained that structured logs most of a time carries all the raw data, that eventually is recorded into prometheus server in some sorts
of aggregations. Eg: logs can record every http 500 request along with country of origin, while promethues may scrape metrics that only counts number of 500
So in theory one could use only structured logs and tools like elasticsearch to perform data aggregations, and than alerts on them, the same way as
it is done on prometheus. However, indexing larg volumes of logs is way more costly than keeping pre aggregated data, also indexing structured logs is 
more complex proces, and therefore less stable. 

Ask Ben on Monday

## Incidents

### Missleading prometheus metrics collection (gaps in data) for cloudflare

We collects metrics from cloudfalre (dns provide) through [exporter](https://gitlab.com/gitlab-org/cloudflare_exporter) which scrapes its GraphQL API 
to fetch bucketed data about requests (eg: #200 and #500 status codes). This buckets are updated based on fequency of occurances
of their members, and for some buckets updates happens very rare. That means some values stays the same over long period of time > 5min.

That caused plotted charts from prometheus to be very spiky (https://gitlab.com/gitlab-org/cloudflare_exporter/-/issues/2) 

![spiky chart](img/cloudflare_spiky_chart.png)

#### Why spikness was an issue how timestamping was meant to help?

Mostly matter of preference, it was replaced with discreet data, on a change.

#### Why timestamping did not met expectations?

It causes data to be updated ocassionaly and if this time between updates exceeds the rolling 5m rate that we use for graphing, 
then it appears that there is no data, even though there is.

It's worth noting however, that the alert was valid
Checking the cloudflare dashboard, the problem is visible there, as shown in the spike in 502 Bad Gateway and 521 Origin Down status codes:

![error chart from cloudflare](img/cloudflare_error_spike.png)

but it was plotted like

![how gitlab plots it](img/cloudflare_error_spike_chart.png)

#### What is timestamping data in prometheus

Normaly prometheus exporters presents metrics data without additional information about when it was recorded, 
and promethues server assumes that scrape time of metric should be used in that case. However if needed
prometheus exporter can present timestamp along metrics value, and in that case prometheus server use that timestamp
when recording the data.

default metrics
```
# HELP namespace_subsystem_name help
# TYPE namespace_subsystem_name counter
namespace_subsystem_name{const="constval",l1="banana",l2="potato"} 100
```


timestamped metrics
```
# HELP namespace_subsystem_name help
# TYPE namespace_subsystem_name counter
namespace_subsystem_name{const="constval",l1="banana",l2="potato"} 100 1589796425000
```

#### How it is planned to be addresed

https://gitlab.com/gitlab-com/gl-infra/infrastructure/-/issues/10926

>The best course of action may be to update timestamps for all time series in a dataset when any of them have updated data. That way, rates of infrequently changing counters will go to zero rather than disappear, and we can remain accurate to the times of events because cloudflare metrics sometimes lag quite a bit.

>The downside to this would be spiky rates instead of discrete, spaced-out data points, and fuzzy attribution of the exact time of events (e.g. if cloudflare had not returned new data for, e.g., 10 mins, then returns a data point from 5 mins ago - it'd be attributed to scrape time).
